varying  vec3 texCoord;
uniform int TextureFlag;
uniform int colorFlag;
uniform int phongChoice;
varying  vec3 fN,fN_point;
varying  vec3 fV,fV_point;
varying  vec3 fL,fL_point;
varying  vec4 color;

uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform vec4  DiffuseProduct_point, SpecularProduct_point;
uniform vec4 LightPosition,LightPosition_point;
uniform float Shininess,Shininess_point;

#define M_PI  3.14159265358979323846

uniform sampler2D texture;

void main()
{
    if (TextureFlag == 1)
    {
//        Shading the texture object with phong without any color
        vec3 N = normalize(fN);
        vec3 V = normalize(fV);
        vec3 L = normalize(fL);
        vec3 H;
        vec3 H_point;
        
        vec3 N_point = normalize(fN_point);
        vec3 V_point = normalize(fV_point);
        vec3 L_point = normalize(fL_point);
        if(phongChoice == 1)
        {
            H = normalize( L + V );
            H_point = normalize( L_point + V_point );
        }
        else
        {
            H = normalize(2.0* N * max(dot(L, N), 0.0) - L );
            H_point = normalize(2.0*N_point * max(dot(L_point,N_point),0.0) - L_point);
        }
        
        
//        vec3 H_point = normalize( L_point + V_point );
        
        vec4 ambient = AmbientProduct;
        
        float Kd = max(dot(L, N), 0.0);
        vec4 diffuse = Kd*DiffuseProduct;
        
        float Kd_point = max(dot(L_point, N_point), 0.0);
        vec4 diffuse_point = Kd_point*DiffuseProduct_point;
        
        float Ks = pow(max(dot(N, H), 0.0), Shininess);
        vec4 specular = Ks*SpecularProduct;
        
        float Ks_point = pow(max(dot(N_point, H_point), 0.0), Shininess_point);
        vec4 specular_point = Ks_point*SpecularProduct_point;
        
        // discard the specular highlight if the light's behind the vertex
        if( dot(L, N) < 0.0 ) {
            specular = vec4(0.0, 0.0, 0.0, 1.0);
        }
        
        if( dot(L_point, N_point) < 0.0 ) {
            specular_point = vec4(0.0, 0.0, 0.0, 1.0);
        }
        vec2 text_sphere = vec2((atan(texCoord.y, texCoord.x) / M_PI + 1.0) * 0.5,
                         (asin(texCoord.z) / M_PI + 0.5));
        gl_FragColor = (ambient + diffuse + specular + diffuse_point + specular_point)*texture2D( texture, text_sphere );
        gl_FragColor.a = 1.0;
   
    }
    else if(TextureFlag == 2)
    {
//      Default Phong Shading Rules
        vec3 N = normalize(fN);
        vec3 V = normalize(fV);
        vec3 L = normalize(fL);
        
        vec3 H = normalize( L + V );
        
        vec3 N_point = normalize(fN_point);
        vec3 V_point = normalize(fV_point);
        vec3 L_point = normalize(fL_point);
        
        vec3 H_point = normalize( L_point + V_point );
        
        vec4 ambient = AmbientProduct;
        
        float Kd = max(dot(L, N), 0.0);
        vec4 diffuse = Kd*DiffuseProduct;
        
        float Kd_point = max(dot(L_point, N_point), 0.0);
        vec4 diffuse_point = Kd_point*DiffuseProduct_point;
        
        float Ks = pow(max(dot(N, H), 0.0), Shininess);
        vec4 specular = Ks*SpecularProduct;
        
        float Ks_point = pow(max(dot(N_point, H_point), 0.0), Shininess_point);
        vec4 specular_point = Ks_point*SpecularProduct_point;
        
        if( dot(L, N) < 0.0 ) {
            specular = vec4(0.0, 0.0, 0.0, 1.0);
        }
        
        if( dot(L_point, N_point) < 0.0 ) {
            specular_point = vec4(0.0, 0.0, 0.0, 1.0);
        }
        
        if(colorFlag == 1)
        {
            gl_FragColor = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,0.0,0.0,1.0);
        }
        else if(colorFlag == 2)
        {
            gl_FragColor = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(0.0,1.0,0.0,1.0);
        }
        else if(colorFlag == 3)
        {
            gl_FragColor = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,0.0,1.0,1.0);
        }
        else if(colorFlag == 4)
        {
            gl_FragColor = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,1.0,0.0,0.0);
        }
        else if(colorFlag == 5)
        {
            gl_FragColor = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(0.0,1.0,1.0,1.0);
        }
        else if(colorFlag == 6)
        {
            gl_FragColor = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(0.0,0.0,1.0,1.0);
        }
        else if(colorFlag == 7)
        {
            gl_FragColor = (ambient + diffuse + specular + diffuse_point + specular_point)* vec4(1.0,1.0,1.0,1.0);
        }
        else if(colorFlag == 8)
        {
            gl_FragColor = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(0.0,0.0,0.0,1.0);
        }
        else
        {
            gl_FragColor = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,0.0,0.0,1.0);
        }
        gl_FragColor.a = 1.0;
        /*
        switch(colorFlag)
        {
            case 1:
                gl_FragColor = (ambient + diffuse + specular)* vec4(1.0,0.0,0.0,1.0);
                break;
            case 2:
                gl_FragColor = (ambient + diffuse + specular)* vec4(0.0,1.0,0.0,1.0);
                break;
            case 3:
                gl_FragColor = (ambient + diffuse + specular)* vec4(1.0,0.0,1.0,1.0);
                break;
            case 4:
                gl_FragColor = (ambient + diffuse + specular)* vec4(1.0,1.0,0.0,0.0);
                break;
            case 5:
                gl_FragColor = (ambient + diffuse + specular)* vec4(0.0,1.0,1.0,1.0);
                break;
            case 6:
                gl_FragColor = (ambient + diffuse + specular)* vec4(0.0,0.0,1.0,1.0);
                break;
            case 7:
                gl_FragColor = (ambient + diffuse + specular)* vec4(1.0,1.0,1.0,1.0);
                break;
            case 8:
                gl_FragColor = (ambient + diffuse + specular)* vec4(0.0,0.0,0.0,1.0);
                break;
            default:
                break;
        }
         */
    }
    else if(TextureFlag == 3)
    {
//        Default Gouraud shading rules
        gl_FragColor = color;
        gl_FragColor.a = 1.0;
    }
    else if (TextureFlag == 4)
    {
//        No shading texture draw
        vec2 text_sphere = vec2((atan(texCoord.y, texCoord.x) / 3.1415926 + 1.0) * 0.5,
                                (asin(texCoord.z) / 3.1415926 + 0.5));
        gl_FragColor = texture2D( texture, text_sphere );
    }
    else if (TextureFlag == 5)
    {
        //        Gouraud shading texture draw if the object is texturized
        vec2 text_sphere = vec2((atan(texCoord.y, texCoord.x) / M_PI + 1.0) * 0.5,
                                (asin(texCoord.z) / M_PI + 0.5));
        gl_FragColor = texture2D( texture, text_sphere )* color;
    }
    else
    {
        gl_FragColor = color;
        gl_FragColor.a = 1.0;
    }
    
} 

