attribute  vec4 vPosition;
attribute  vec3 vTexCoord;
//attribute  vec3 osg_norm;
//attribute  vec3 osg_Vertex;

attribute   vec3 vNormal;

varying  vec3 fN,fN_point;
varying  vec3 fV,fV_point;
varying  vec3 fL,fL_point;
varying vec4 color;

uniform mat4 ModelView;
uniform vec4 LightPosition,LightPosition_point;
uniform mat4 Projection;
uniform int shading;
uniform int colorFlag;

uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform vec4 AmbientProduct_point,DiffuseProduct_point,SpecularProduct_point;
uniform float Shininess, Shininess_point;

varying vec3 texCoord;

/*
vec2 sphereMap(const attribute vec4 ecPos,const attribute vec3 normal)
{
    float m;
    vec3 r,u;
    u = normalise(vec3(ecPos)/ecPos.w);
    r = reflect(u,normal);
    r.z += 1.0;
    m = 0.5*inversesqrt(dot(r,r));
    return r.xy * m + 0.5;
}
*/
void main()
{
    if(shading == 1)
    {
//        Phong shading
        fN = (ModelView*vec4(vNormal, 0.0)).xyz; // normal direction in camera coordinates
        fN_point = (ModelView*vec4(vNormal, 0.0)).xyz;
        
        fV = (ModelView * vPosition).xyz; //viewer direction in camera coordinates
        fV_point =(ModelView * vPosition).xyz;
        
        fL = LightPosition.xyz; // light direction
        fL_point = LightPosition_point.xyz;
        
        if( LightPosition.w != 0.0 ) {
            fL = LightPosition.xyz - fV;  //directional light source
        }
        
        if( LightPosition_point.w != 0.0 ) {
            fL_point = LightPosition_point.xyz - fV_point;  //directional light source
        }
    }
    else if(shading == 2)
    {
//        Gouraud shading
//        Transform vertex  position into camera (eye) coordinates
        vec3 pos = (ModelView * vPosition).xyz;
        vec3 pos_point = (ModelView * vPosition).xyz;
        /*
        LightPosition.x = 1000.00*LightPosition.x;
        LightPosition.y =LightPosition.y*1000.00;
        LightPosition.z =LightPosition.z*1000.00;
        
        LightPosition_point.x = 1000.00*LightPosition_point.x;
        LightPosition_point.y =LightPosition_point.y*1000.00;
        LightPosition_point.z =LightPosition_point.z*1000.00;
        */
        vec3 L = normalize( LightPosition.xyz - pos ); //light direction
        vec3 V = normalize( pos ); // viewer direction
        vec3 H = normalize( L + V ); // halfway vector
        
        vec3 L_point = normalize( LightPosition_point.xyz - pos_point ); //light direction
        vec3 V_point = normalize( pos_point ); // viewer direction
        vec3 H_point = normalize( L_point + V_point ); // halfway vector
        
//        Transform vertex normal into camera coordinates
        vec3 N = normalize( ModelView * vec4(vNormal, 0.0) ).xyz;
        vec3 N_point = normalize( ModelView * vec4(vNormal, 0.0) ).xyz;
        
//        Compute terms in the illumination equation
        vec4 ambient = AmbientProduct;
        
        float Kd = max( dot(L, N), 0.0 ); //set diffuse to 0 if light is behind the surface point
        vec4  diffuse = Kd*DiffuseProduct;
        
        float Kd_point = max( dot(L_point, N_point), 0.0 ); //set diffuse to 0 if light is behind the surface point
        vec4  diffuse_point = Kd_point*DiffuseProduct_point;
        
        float Ks = pow( max(dot(N, H), 0.0), Shininess );
        vec4  specular = Ks * SpecularProduct;

        float Ks_point = pow( max(dot(N_point, H_point), 0.0), Shininess_point );
        vec4  specular_point = Ks_point * SpecularProduct_point;

        
//        ignore also specular component if light is behind the surface point
        if( dot(L, N) < 0.0 ) {
            specular = vec4(0.0, 0.0, 0.0, 1.0);
        }
        
        if( dot(L_point, N_point) < 0.0 ) {
            specular_point = vec4(0.0, 0.0, 0.0, 1.0);
        }
        
        if(colorFlag == 1)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,0.0,0.0,1.0);
        }
        else if(colorFlag == 2)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(0.0,1.0,0.0,1.0);
        }
        else if(colorFlag == 3)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,0.0,1.0,1.0);
        }
        else if(colorFlag == 4)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,1.0,0.0,0.0);
        }
        else if(colorFlag == 5)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(0.0,1.0,1.0,1.0);
        }
        else if(colorFlag == 6)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(0.0,0.0,1.0,1.0);
        }
        else if(colorFlag == 7)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,1.0,1.0,1.0);
        }
        else if(colorFlag == 8)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(0.0,0.0,0.0,1.0);
        }
        else
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point)* vec4(1.0,0.0,0.0,1.0);
        }
        color.a = 1.0;

    }
    else if (shading == 3)
    {
        vec3 pos = (ModelView * vPosition).xyz;
        vec3 pos_point = (ModelView * vPosition).xyz;
        
        vec3 L = normalize( LightPosition.xyz - pos ); //light direction
        vec3 V = normalize( pos ); // viewer direction
        vec3 H = normalize( L + V ); // halfway vector
        
        vec3 L_point = normalize( LightPosition_point.xyz - pos_point ); //light direction
        vec3 V_point = normalize( pos_point ); // viewer direction
        vec3 H_point = normalize( L_point + V_point ); // halfway vector
        
//        Transform vertex normal into camera coordinates
        vec3 N = normalize( ModelView * vec4(vNormal, 0.0) ).xyz;
        vec3 N_point = normalize( ModelView * vec4(vNormal, 0.0) ).xyz;
        
//        Compute terms in the illumination equation
        vec4 ambient = AmbientProduct;
        vec4 ambient_point = AmbientProduct_point;
        
        float Kd = max( dot(L, N), 0.0 ); //set diffuse to 0 if light is behind the surface point
        vec4  diffuse = Kd*DiffuseProduct;
        
        float Kd_point = max( dot(L_point, N_point), 0.0 ); //set diffuse to 0 if light is behind the surface point
        vec4  diffuse_point = Kd_point*DiffuseProduct_point;
        
        float Ks = pow( max(dot(N, H), 0.0), Shininess );
        vec4  specular = Ks * SpecularProduct;
        
        float Ks_point = pow( max(dot(N_point, H_point), 0.0), Shininess_point );
        vec4  specular_point = Ks_point * SpecularProduct_point;
        
        
//        ignore also specular component if light is behind the surface point
        if( dot(L, N) < 0.0 ) {
            specular = vec4(0.0, 0.0, 0.0, 1.0);
        }
        
        if( dot(L_point, N_point) < 0.0 ) {
            specular_point = vec4(0.0, 0.0, 0.0, 1.0);
        }
        
        if(colorFlag == 1)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        else if(colorFlag == 2)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        else if(colorFlag == 3)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        else if(colorFlag == 4)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        else if(colorFlag == 5)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        else if(colorFlag == 6)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        else if(colorFlag == 7)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        else if(colorFlag == 8)
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        else
        {
            color = (ambient + diffuse + specular  + diffuse_point + specular_point);
        }
        color.a = 1.0;
    }
    if(shading == 0)
    {
//        No shading default colors 
        if(colorFlag == 1)
        {
            color =  vec4(1.0,0.0,0.0,1.0);
        }
        else if(colorFlag == 2)
        {
            color = vec4(0.0,1.0,0.0,1.0);
        }
        else if(colorFlag == 3)
        {
            color = vec4(1.0,0.0,1.0,1.0);
        }
        else if(colorFlag == 4)
        {
            color = vec4(1.0,1.0,0.0,0.0);
        }
        else if(colorFlag == 5)
        {
            color =vec4(0.0,1.0,1.0,1.0);
        }
        else if(colorFlag == 6)
        {
            color = vec4(0.0,0.0,1.0,1.0);
        }
        else if(colorFlag == 7)
        {
            color = vec4(1.0,1.0,1.0,1.0);
        }
        else if(colorFlag == 8)
        {
            color =  vec4(0.0,0.0,0.0,1.0);
        }
        else
        {
            color =  vec4(1.0,0.0,0.0,1.0);
        }
         color.a = 1.0;
    }

    texCoord    = vTexCoord;
    gl_Position = Projection * ModelView * vPosition;
}
