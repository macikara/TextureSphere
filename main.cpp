
#include "Angel.h"

/*
 Designed By Mustafa Acıkaraoğlu
 0029036
 */

const int NumTimesToSubdivide = 5;
const int NumTrianglesCircle = 4096;  // (4 faces)^(NumTimesToSubdivide + 1)
const int NumVerticesCircle = 3 * NumTrianglesCircle;

typedef Angel::vec4 point4;
typedef Angel::vec4 color4;

int TextureSize = 0;

color4 sphere_colors[NumVerticesCircle];
point4 circlePoints[NumVerticesCircle];
vec3   normals[NumVerticesCircle];
vec3   tex_coordinates[NumVerticesCircle];

GLuint program;

GLuint textures[2];
// Vertices of a unit cube centered at origin, sides aligned with axes

enum { Xaxis = 0, Yaxis = 1, Zaxis = 2, NumAxes = 3 };
int  Axis = Xaxis;
GLfloat  Theta[NumAxes] = { 15.0, 30.0, 0.0 };

GLuint  ModelView, Projection;
GLfloat rotateSpeed = 0.0;

double widthOfCircle = 2.0;
int textureChoice = 1;
int shadingChoice = 1;

double camera_pos = -4.90;
//const double camera_view = -3.0;

vec3 displacement(0.0, 1.5, 0.0);
vec3 velocity(0.0, 0.0, 0.0);

GLubyte* image_1D_earth;
GLubyte* image_1D_basketball;
void ReadPPM_earth()
{
    std::cout << "Reading Earth.ppm" << std::endl;
    FILE *fd;
    int k,n,m;
    char c;
    int i;
    char b[]= "earth.ppm";
    int red,blue,green;
    fd = fopen(b,"r");
    if(fd == NULL)
    {
        std::cout << "Cant open file !"<<std::endl;
        exit(1);
    }
    fscanf(fd,"%[^\n] ",b);
    if(b[0]!='P'|| b[1] != '3')
    {
        printf("Not a ppm file.");
        exit(1);
    }
    printf("Valid file.");
    fscanf(fd, "%c",&c);
    while(c == '#')
    {
        fscanf(fd,"%[^\n] ",b);
        printf("%s\n",b);
        fscanf(fd, "%c",&c);
    }
    ungetc(c,fd);
    fscanf(fd, "%d %d %d", &n, &m, &k);
    printf("%d rows %d columns max value= %d\n",n,m,k);
    int nm = n*m;
    image_1D_earth = (GLubyte*)malloc(3*sizeof(GLubyte)*nm);
    for(i=nm;i>0;i--)
    {
        fscanf(fd,"%d %d %d",&red, &green, &blue );
        image_1D_earth[3*nm-3*i]=red;
        image_1D_earth[3*nm-3*i+1]=green;
        image_1D_earth[3*nm-3*i+2]=blue;
    }
    
    
}
void ReadPPM_basketball()
{
    std::cout << "Reading Basketball.ppm" << std::endl;
    FILE *fd;
    int k,n,m;
    char c;
    int i;
    char b[]= "basketball.ppm";
    int red,blue,green;
    fd = fopen(b,"r");
    if(fd == NULL)
    {
        std::cout << "Cant open file !"<<std::endl;
        exit(1);
    }
    fscanf(fd,"%[^\n] ",b);
    if(b[0]!='P'|| b[1] != '3')
    {
        printf("Not a ppm file.");
        exit(1);
    }
    printf("Valid file.");
    fscanf(fd, "%c",&c);
    while(c == '#')
    {
        fscanf(fd,"%[^\n] ",b);
        printf("%s\n",b);
        fscanf(fd, "%c",&c);
    }
    ungetc(c,fd);
    fscanf(fd, "%d %d %d", &n, &m, &k);
    printf("%d rows %d columns max value= %d\n",n,m,k);
    int nm = n*m;
    image_1D_basketball = (GLubyte*)malloc(3*sizeof(GLubyte)*nm);
    for(i=nm;i>0;i--)
    {
        fscanf(fd,"%d %d %d",&red, &green, &blue );
        image_1D_basketball[3*nm-3*i]=red;
        image_1D_basketball[3*nm-3*i+1]=green;
        image_1D_basketball[3*nm-3*i+2]=blue;
    }
    
    
}
/*Original PPM file reader from the slides modified to read two different .ppm files basketball and world
GLubyte* image;

void read_ppm_file(GLubyte* image)
{
    FILE *fd;
    int k,n,m,nm;
    char c;
    int i;
    int red,green,blue;
    char b[] = "/Users/mustafa/Desktop/basketball.ppm";
    printf("\n");
    fd = fopen(b,"r");
    fscanf(fd,"%[^\n] ",b);
    if(b[0]!='P'|| b[1] != '3'){
        std::cout << b[0] << std::endl;
        std::cout << b[1] << std::endl;
        printf("Not valid file\n");
        exit(1);
    }
    printf("Valid file\n");
    fscanf(fd, "%c",&c);
    while(c == '#'){
        fscanf(fd,"%[^\n]",b);
        printf("%s\n",b);
        fscanf(fd, "%c",&c);
    }
    ungetc(c,fd);
    fscanf(fd, "%d %d %d", &n, &m, &k);
    printf("%d rows %d columns max value= %d\n",n,m,k);
    nm = n*m;
    image = (GLubyte*)malloc(3*sizeof(GLuint)*nm);
    TextureSize = 3*sizeof(GLuint)*nm;
    for(i=nm;i>0;i--){
        fscanf(fd,"%d %d %d",&red, &green, &blue );
        image[3*nm-3*i]=red;
        image[3*nm-3*i+1]=green;
        image[3*nm-3*i+2]=blue;
    }
}
*/
//----------------------------------------------------------------------------

int IndexCircle = 0;
color4 temporary = { 1,1,0,1};

void triangle(const point4& a, const point4& b, const point4& c)
{
	vec3  normal = normalize(cross(b - a, c - b));
    normals[IndexCircle] = normal;  circlePoints[IndexCircle] = a; sphere_colors[IndexCircle] = temporary; tex_coordinates[IndexCircle] = vec3(a.x,a.y,a.z); IndexCircle++;
    normals[IndexCircle] = normal;  circlePoints[IndexCircle] = b; sphere_colors[IndexCircle] = temporary; tex_coordinates[IndexCircle] = vec3(b.x,b.y,b.z); IndexCircle++;
	normals[IndexCircle] = normal;  circlePoints[IndexCircle] = c; sphere_colors[IndexCircle] = temporary; tex_coordinates[IndexCircle] = vec3(c.x,c.y,c.z); IndexCircle++;
//    std::cout << IndexCircle << std::endl;
}

//----------------------------------------------------------------------------

point4 unit(const point4& p)
{
	float len = p.x*p.x + p.y*p.y + p.z*p.z;
	point4 t;
	if (len > DivideByZeroTolerance) {
		t = p / sqrt(len);
		t.w = widthOfCircle;
	}

	return t;
}

void divide_triangle(const point4& a, const point4& b, const point4& c, int count)
{
	if (count > 0) {
		point4 v1 = unit(a + b);
		point4 v2 = unit(a + c);
		point4 v3 = unit(b + c);
		divide_triangle(a, v1, v2, count - 1);
		divide_triangle(c, v2, v3, count - 1);
		divide_triangle(b, v3, v1, count - 1);
		divide_triangle(v1, v3, v2, count - 1);
	}
	else {
		triangle(a, b, c);
	}
}

void tetrahedron(int count)
{
	point4 v[4] = {
		vec4(0.0, 0.0, 1.0, widthOfCircle),
		vec4(0.0, 0.942809, -0.333333, widthOfCircle),
		vec4(-0.816497, -0.471405, -0.333333, widthOfCircle),
		vec4(0.816497, -0.471405, -0.333333, widthOfCircle)
	};

	divide_triangle(v[0], v[1], v[2], count);
	divide_triangle(v[3], v[2], v[1], count);
	divide_triangle(v[0], v[3], v[1], count);
	divide_triangle(v[0], v[2], v[3], count);
}

//----------------------------------------------------------------------------
GLuint TextureFlag,colorFlag,Shading,phongChoice;
bool textureFlag = true;
GLuint vColor;
point4 light_position( 3.0, -4.0, 5.0/*+camera_pos+camera_view*/, 0.0 );
color4 light_ambient( 0.2, 0.2, 0.2, 1.0 );
color4 light_diffuse( 1.0, 1.0, 1.0, 1.0 );
color4 light_specular( 1.0, 1.0, 1.0, 1.0 );

color4 material_ambient( 1.0, 0.0, 1.0, 1.0 );
color4 material_diffuse( 1.0, 0.8, 0.4, 1.0 );
color4 material_specular( 1.0, 0.8, 0.0, 1.0 );
float  material_shininess = 100.0;

point4 light_position_point( -0.5, 1.0, 2.0/*+camera_pos+camera_view*/, 1.0 );
color4 light_ambient_point( 0.2, 0.2, 0.2, 1.0 );
color4 light_diffuse_point( 1.0, 1.0, 1.0, 1.0 );
color4 light_specular_point( 1.0, 1.0, 1.0, 1.0 );

color4 material_ambient_point( 1.0, 0.0, 1.0, 1.0 );
color4 material_diffuse_point( 1.0, 0.8, 0.4, 1.0 );
color4 material_specular_point( 1.0, 0.8, 0.0, 1.0 );
float  material_shininess_point = 100.0;

color4 ambient_product = light_ambient * material_ambient;
color4 diffuse_product = light_diffuse * material_diffuse;
color4 specular_product = light_specular * material_specular;

color4 ambient_product_point = light_ambient_point * material_ambient_point;
color4 diffuse_product_point = light_diffuse_point * material_diffuse_point;
color4 specular_product_point = light_specular_point * material_specular_point;

void init()
{
//    std::cout << image_1D[0] << std::endl;
    
    tetrahedron(NumTimesToSubdivide);
//    std::cout << tex_coordinates[2] << std::endl;
    
    glGenTextures( 2, textures );
    
    glBindTexture( GL_TEXTURE_2D, textures[0] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, 2048, 1024, 0,
                 GL_RGB, GL_UNSIGNED_BYTE, image_1D_earth );
    glGenerateMipmap(GL_TEXTURE_2D);
    
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
//    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_REPEAT );
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_LINEAR);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    
    glBindTexture( GL_TEXTURE_2D, textures[1] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, 512, 256, 0,
                 GL_RGB, GL_UNSIGNED_BYTE, image_1D_basketball );
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    
    glBindTexture( GL_TEXTURE_2D, textures[0] );
    
    // Create a vertex array object
    GLuint vao;
    glGenVertexArrays( 1, &vao );
    glBindVertexArray( vao );

    // Create and initialize a buffer object
    
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(circlePoints) + sizeof(sphere_colors)+sizeof(normals)+sizeof(tex_coordinates),
                 NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(circlePoints), circlePoints);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(circlePoints), sizeof(normals), normals);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(circlePoints)+sizeof(tex_coordinates), sizeof(tex_coordinates), tex_coordinates);
    
//    Load shaders and use the resulting shader program
    program = InitShader( "vshader.glsl", "fshader.glsl" );
    glUseProgram( program );

    GLintptr offset = 0;
    GLuint vPosition = glGetAttribLocation( program, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,
                          BUFFER_OFFSET(offset));
    offset += sizeof(circlePoints);
    
    GLuint vNormal = glGetAttribLocation( program, "vNormal" );
    glEnableVertexAttribArray( vNormal );
    glVertexAttribPointer( vNormal, 3, GL_FLOAT, GL_FALSE, 0,
                          BUFFER_OFFSET(offset) );
    
    offset += sizeof(normals);
    
    GLuint vTexCoord = glGetAttribLocation( program, "vTexCoord" );
    glEnableVertexAttribArray( vTexCoord );
    glVertexAttribPointer( vTexCoord, 3, GL_FLOAT, GL_FALSE, 0,
                          BUFFER_OFFSET(offset) );
//    std::cout << sizeof(tex_coordinates) << std::endl;
    
    ModelView = glGetUniformLocation(program, "ModelView");
    Projection = glGetUniformLocation(program, "Projection");
    
    TextureFlag = glGetUniformLocation(program, "TextureFlag");
    glUniform1i( TextureFlag, textureChoice );
    
    colorFlag = glGetUniformLocation(program, "colorFlag");
    glUniform1i(colorFlag,1);
    
    Shading = glGetUniformLocation(program, "shading");
    glUniform1i(Shading, shadingChoice);

    phongChoice = glGetUniformLocation(program, "phongChoice");
    glUniform1i(phongChoice,1);
    
//    Initialize shader lighting parameters
    
    glUniform4fv( glGetUniformLocation(program, "AmbientProduct"),
                 1, ambient_product );
    glUniform4fv( glGetUniformLocation(program, "DiffuseProduct"),
                 1, diffuse_product );
    glUniform4fv( glGetUniformLocation(program, "SpecularProduct"),
                 1, specular_product );
    
    glUniform4fv( glGetUniformLocation(program, "LightPosition"),
                 1, light_position );
    
    glUniform1f( glGetUniformLocation(program, "Shininess"),
                material_shininess );
    
    glUniform4fv( glGetUniformLocation(program, "DiffuseProduct_point"),
                 1, diffuse_product_point );
    glUniform4fv( glGetUniformLocation(program, "SpecularProduct_point"),
                 1, specular_product_point );
    
    glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
                 1, light_position_point );
    
    glUniform1f( glGetUniformLocation(program, "Shininess_point"),
                material_shininess_point );
    
    glEnable( GL_DEPTH_TEST );
    glEnable(GL_CULL_FACE);

    
    glClearColor( 0.2, 0.2, 0.2, 1.0 );
   }

//----------------------------------------------------------------------------

void display( void )
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mat4  model_view = ( Translate( displacement ) *
                        Scale(1, 1, 1) *
                        RotateX( Theta[Xaxis] ) *
                        RotateY( Theta[Yaxis] ) *
                        RotateZ( Theta[Zaxis] ) );
    
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view );
    //glUniform4f(vColor, 1.0, 0.0, 0.0, 0.0);
	glDrawArrays(GL_TRIANGLES, 0, NumVerticesCircle);
    glutSwapBuffers();

}

void reshape(int width, int height)
{
	glViewport(0, 0, width, height);

	GLfloat left = -2.0, right = 2.0;
	GLfloat top = 2.0, bottom = -2.0;
    GLfloat zNear = -20.0, zFar = 20.0;

	GLfloat aspect = GLfloat(width) / height;

	if (aspect > 1.0) 
	{
		left *= aspect;
		right *= aspect;
	}
	else 
	{
		top /= aspect;
		bottom /= aspect;
	}

    mat4 projection = Ortho(left, right, bottom, top, zNear, zFar);
//    mat4 projection = Perspective(45, aspect , 1, 200);
	glUniformMatrix4fv(Projection, 1, GL_TRUE, projection);
}


//----------------------------------------------------------------------------

void idle( void )
{
    Theta[Axis] += 0.7;
    if ( Theta[Axis] > 360.0 ) 
	{
        Theta[Axis] -= 360.0;
    }
    glutPostRedisplay();
}

//----------------------------------------------------------------------------
void specialArrowFunc(int key,int x,int y)
{
	if (key == GLUT_KEY_UP) 
	{
		if (velocity.y >= 0.1) {
			velocity.y = 0.1;
		}
		else {
			velocity.y += 0.01;
		}
	}
	if(key == GLUT_KEY_DOWN)
	{
		if (velocity.y <= -0.1) {
			velocity.y = -0.1;
		}
		else {
			velocity.y -= 0.01;
		}
	}
}
void keyboard( unsigned char key,int x, int y )
{
	if (key == 'h' || key == 'H')
	{
        std::cout << "\t\t\tOptions\nPress p or P to re-color the object(Can only do this in non texture drawing.\nPress q or Q to exit.\nPress UP-ARROW to increase the speed, DOWN-ARROW to decrease the speed.\nPress e to instantly stop the velocity of the moving object.\nUse the W-A-S-D keys to move the point light source.\nEnjoy." << std::endl;
	}
	if (key == 'Q' || key == 'q') 
	{
		exit(0);
	} 
	if (key == 'p' || key == 'P') 
	{
        int x = rand()%7;
        x = x+1;
        glUniform1i(colorFlag,x);
	}
	if (key == 'b' ||key == 'B') 
	{
		//rotateSpeed += 0.005;
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	if (key == 'g' || key == 'G')
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
    if (key == 'g' || key == 'G')
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    switch (key)
	{
	case 'e':
		velocity.y = 0;
		break;
    case 'w':
            light_position_point.y += 1.0;
            glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
                         1, light_position_point );
        break;
    case 's':
            light_position_point.y -= 1.0;
            glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
                         1, light_position_point );
        break;
    case 'a':
            light_position_point.x -= 1.0;
            glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
                         1, light_position_point );
        break;
    case 'd':
            light_position_point.x += 1.0;
            glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
                         1, light_position_point );
        break;
	default:
		break;
	}
}

//----------------------------------------------------------------------------
double alterate = 1;
bool light_moves = false;
//----------------------------------------------------------------------------
const vec3 gravity(0.0, -(9.8 * 5 / 100000), 0.0);
void timer( int p )
{
	if (displacement.y >= 1.5 || displacement.y <= -1.5)
    {
		velocity.y = -velocity.y;
	}
	displacement += velocity += gravity;
    if(light_moves == true)
    {
//        std::cout << "Light is moving" << std::endl;
//        light_position_point.y += velocity.y += gravity.y;
//        std::cout << light_position_point << std::endl;
        light_position.y = velocity.y+light_position.y + gravity.y;
//        light_position += velocity += gravity;
//        glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
//                     1, light_position_point );
        glUniform4fv( glGetUniformLocation(program, "LightPosition"),
                     1, light_position );
    }
	if (displacement.y <= -1.50002)
    {
		displacement.y = -1.5;
	}
	//displacement+= velocity += gravity;
    glutPostRedisplay();

    glutTimerFunc(5,timer,0);
}

void reinitialiseBufferSphere(void)
{
	IndexCircle = 0;
	tetrahedron(NumTimesToSubdivide);

    
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(circlePoints),
		sizeof(sphere_colors), sphere_colors);

}

void color_menu(int index)
{
	switch (index)
	{
	case 1: 
	{
//        temporary = { 1,0,0,1 };//
//        reinitialiseBufferSphere();
        shadingChoice = 1;
        textureChoice = 2;
        glUniform1i(colorFlag,1);
        glUniform1i(Shading,shadingChoice);
        glUniform1i(TextureFlag,textureChoice);
		
	}break;

	case 2:
	{
//        temporary = { 0,1,0,1 };//
//        reinitialiseBufferSphere();
        shadingChoice = 1;
        textureChoice = 2;
        glUniform1i(colorFlag,2);
        glUniform1i(Shading,shadingChoice);
        glUniform1i(TextureFlag,textureChoice);
	}break;

	case 3:
	{
//        temporary = { 1,0,1,1 };//
//        reinitialiseBufferSphere();
        shadingChoice = 1;
        textureChoice = 2;
        glUniform1i(colorFlag,3);
        glUniform1i(Shading,shadingChoice);
        glUniform1i(TextureFlag,textureChoice);
	}break;

	case 4:
	{
//        temporary = { 1,1,0,1 };//
//        reinitialiseBufferSphere();
        shadingChoice = 1;
        textureChoice = 2;
        glUniform1i(colorFlag,4);
        glUniform1i(Shading,shadingChoice);
        glUniform1i(TextureFlag,textureChoice);
	}break;

	case 5:
	{
//        temporary = { 0,1,1,1 };//
//        reinitialiseBufferSphere();
        shadingChoice = 1;
        textureChoice = 2;
        glUniform1i(colorFlag,5);
        glUniform1i(Shading,shadingChoice);
        glUniform1i(TextureFlag,textureChoice);
	}break;

	case 6:
	{
//        temporary = { 0,0,1,1 };//
//        reinitialiseBufferSphere();
        shadingChoice = 1;
        textureChoice = 2;
        glUniform1i(colorFlag,6);
        glUniform1i(Shading,shadingChoice);
        glUniform1i(TextureFlag,textureChoice);
	}break;

	case 7:
	{
//        temporary = { 1,1,1,1 };//
//        reinitialiseBufferSphere();
        shadingChoice = 1;
        textureChoice = 2;
        glUniform1i(colorFlag,7);
        glUniform1i(Shading,shadingChoice);
        glUniform1i(TextureFlag,textureChoice);
	}break;

	case 8:
	{
//        temporary = { 0,0,0,1 };//
//        reinitialiseBufferSphere();
        shadingChoice = 1;
        textureChoice = 2;
        glUniform1i(colorFlag,8);
        glUniform1i(Shading,shadingChoice);
        glUniform1i(TextureFlag,textureChoice);
	}break;

	}
}
void earth_or_basketball(int index)
{
    switch(index)
    {
        case 1:
            glBindTexture( GL_TEXTURE_2D, textures[0] );
            break;
        case 2:
            glBindTexture( GL_TEXTURE_2D, textures[1] );
            break;
        default:
            break;
    }
}

void draw_type(int index)
{
	switch (index)
	{
	case 1:
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	case 2:
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        
		break;
    case 3:
            glUniform1i( Shading, 1 );
            glUniform1i( TextureFlag, 1 );
            shadingChoice = 1;
            textureChoice = 1;
            glBindTexture( GL_TEXTURE_2D, textures[1] );
        break;
    case 4:
            glUniform1i( TextureFlag, 1 );
            glUniform1i( Shading, 1 );
            shadingChoice = 1;
            textureChoice = 1;
            glBindTexture( GL_TEXTURE_2D, textures[0] );
        break;
	default:
		break;
	}
}
void _shading_type(int index)
{
    switch(index)
    {
        case 1:
            light_position.w = 0;
            light_position.x = light_position.x/10000;
            light_position.y = light_position.y/10000;
            light_position.z = light_position.z/10000;
            glUniform4fv( glGetUniformLocation(program, "LightPosition"),
                         1, light_position );
            glUniform1i(phongChoice,1);
            if(textureChoice == 1)
            {
                glUniform1i(Shading, 1);
                glUniform1i(TextureFlag, textureChoice);
            }
            else
            {
                glUniform1i(Shading, 1);
                glUniform1i(TextureFlag, 2);
            }
            break;
        case 2:
            light_position.w = 1;
            light_position.x = 10000*light_position.x;
            light_position.y = 10000*light_position.y;
            light_position.z = 10000*light_position.z;
            glUniform4fv( glGetUniformLocation(program, "LightPosition"),
                         1, light_position );
            if(textureChoice == 1)
            {
                
                glUniform1i(Shading, 3);
                glUniform1i(TextureFlag, 5);
            }
            else
            {
//                std::cout << "entered here !" << std::endl;
                glUniform1i(Shading, 2);
                glUniform1i(TextureFlag, 0);
            }
            break;
        case 3:
        {
            if(textureChoice == 1)
            {
                //textureChoice = 4;
                glUniform1i(TextureFlag, 4);
//                std::cout << "textureChoice is : "<<textureChoice<<std::endl;
            }
            else if(textureChoice == 4)
            {
                //textureChoice = 1;
                glUniform1i(TextureFlag, 1);
//                std::cout << "textureChoice is : "<<textureChoice<<std::endl;
            }
            else
            {
                glUniform1i(Shading, 0);
                glUniform1i(TextureFlag, 0);
//                std::cout << "textureChoice is : "<<textureChoice<<std::endl;
            }
        }
            break;
        case 4:
        {
            light_position.w = 0;
            light_position.x = light_position.x/10000;
            light_position.y = light_position.y/10000;
            light_position.z = light_position.z/10000;
            glUniform4fv( glGetUniformLocation(program, "LightPosition"),
                         1, light_position );
            glUniform1i(phongChoice,0);
            if(textureChoice == 1)
            {
                glUniform1i(Shading, 1);
                glUniform1i(TextureFlag, textureChoice);
            }
            else
            {
                glUniform1i(Shading, 1);
                glUniform1i(TextureFlag, 2);
            }
        }break;
        default:
            break;
    }
}

void _consistency(int index)
{
    switch(index)
    {
        case 1:
            material_shininess_point = 10.0;
            glUniform1f( glGetUniformLocation(program, "Shininess_point"),
                        material_shininess_point );
            material_shininess = 10.0;
            glUniform1f( glGetUniformLocation(program, "Shininess"),
                        material_shininess );
            break;
        case 2:
            material_shininess_point = 100.0;
            glUniform1f( glGetUniformLocation(program, "Shininess_point"),
                        material_shininess_point );
            material_shininess = 100.0;
            glUniform1f( glGetUniformLocation(program, "Shininess"),
                        material_shininess );
            break;
        default:
            break;
    }
}

void _light_switch(int index)
{
    color4 switchOff(0.0,0.0,0.0,0.0);
    switch(index)
    {
//            glUniform4fv( glGetUniformLocation(program, "AmbientProduct"),
//                         1, ambient_product );
//            glUniform4fv( glGetUniformLocation(program, "DiffuseProduct"),
//                         1, diffuse_product );
//            glUniform4fv( glGetUniformLocation(program, "SpecularProduct"),
//                         1, specular_product );
//            
//            glUniform4fv( glGetUniformLocation(program, "LightPosition"),
//                         1, light_position );
//            
//            glUniform1f( glGetUniformLocation(program, "Shininess"),
//                        material_shininess );
//            
//            glUniform4fv( glGetUniformLocation(program, "AmbientProduct_point"),
//                         1, ambient_product_point );
//            glUniform4fv( glGetUniformLocation(program, "DiffuseProduct_point"),
//                         1, diffuse_product_point );
//            glUniform4fv( glGetUniformLocation(program, "SpecularProduct_point"),
//                         1, specular_product_point );
//            
//            glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
//                         1, light_position_point );
//            
//            glUniform1f( glGetUniformLocation(program, "Shininess_point"),
//                        material_shininess_point );
        case 1:
            glUniform4fv( glGetUniformLocation(program, "DiffuseProduct_point"),
                                                  1, diffuse_product_point );
            glUniform4fv( glGetUniformLocation(program, "SpecularProduct_point"),
                                                  1, specular_product_point );
            break;
        case 2:
//            glUniform4fv( glGetUniformLocation(program, "AmbientProduct"),
//                         1, switchOff );
            glUniform4fv( glGetUniformLocation(program, "DiffuseProduct_point"),
                         1, switchOff );
            glUniform4fv( glGetUniformLocation(program, "SpecularProduct_point"),
                         1, switchOff );
            break;
        case 3:
            glUniform4fv( glGetUniformLocation(program, "AmbientProduct"),
                                     1, ambient_product );
            glUniform4fv( glGetUniformLocation(program, "DiffuseProduct"),
                                     1, diffuse_product );
            glUniform4fv( glGetUniformLocation(program, "SpecularProduct"),
                                     1, specular_product );
            break;
        case 4:
            glUniform4fv( glGetUniformLocation(program, "AmbientProduct"),
                                     1, ambient_product );
            glUniform4fv( glGetUniformLocation(program, "DiffuseProduct"),
                                     1, switchOff );
            glUniform4fv( glGetUniformLocation(program, "SpecularProduct"),
                                     1, switchOff );
            break;
        default:
            break;
    }
}

void _light_pos(int index)
{
    switch (index) {
//            glUniform4fv( glGetUniformLocation(program, "LightPosition"),
//                         1, light_position );
//            glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
//                         1, light_position_point );
        case 1:
            light_position_point.x = -light_position_point.x;
            light_position_point.y = -light_position_point.y;
            light_position_point.z = -light_position_point.z;
            glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
                                     1, light_position_point );
            break;
        case 2:
            light_position_point.x = -light_position_point.x;
            light_position_point.y = -light_position_point.y;
            glUniform4fv( glGetUniformLocation(program, "LightPosition_point"),
                         1, light_position_point );
            break;
        case 3:
            light_position.x = -light_position.x;
            light_position.y = -light_position.y;
            light_position.z = -light_position.z;
            glUniform4fv( glGetUniformLocation(program, "LightPosition"),
                                     1, light_position );
            break;
        case 4:
            light_position.x = -light_position.x;
            light_position.y = -light_position.y;
            glUniform4fv( glGetUniformLocation(program, "LightPosition"),
                                     1, light_position );
            break;
        case 5:
            std::cout << std::endl;
            std::cout << "Point-Light : "<< light_position_point.x<< ", " << light_position_point.y << ", "<<light_position_point.z <<std::endl;
            std::cout << "Directional-Light : " << light_position.x << ", " << light_position.y << ", "<<light_position.z << std::endl;
            std::cout << std::endl;
            break;
        case 6:
            light_moves = true;
            break;
        case 7:
            light_moves = false;
            break;
        default:
            break;
    }
}

void main_menu(int index)
{
}
void menuFunc(void)
{
    int __shading_types = glutCreateMenu(_shading_type);
    glutAddMenuEntry("Phong-Modified Shading", 1);
    glutAddMenuEntry("Phong-Normal Shading",4);
    glutAddMenuEntry("Gouraud Shading", 2);
    glutAddMenuEntry("No Shading", 3);
	int _color_menu = glutCreateMenu(color_menu);
	glutAddMenuEntry("Red", 1);
	glutAddMenuEntry("Green", 2);
	glutAddMenuEntry("Magenta", 3);
	glutAddMenuEntry("Yellow", 4);
	glutAddMenuEntry("Cyan", 5);
	glutAddMenuEntry("Blue", 6);
	glutAddMenuEntry("White", 7);
	glutAddMenuEntry("Black", 8);

	int _draw_type = glutCreateMenu(draw_type);
	glutAddMenuEntry("Wireframe", 1);
    glutAddMenuEntry("Solid",2);
    glutAddMenuEntry("Texture Basketball",3);
    glutAddMenuEntry("Texture Earth",4);
    
    int __consistency = glutCreateMenu(_consistency);
    glutAddMenuEntry("Plastic", 1);
    glutAddMenuEntry("Metallic", 2);
    
    int __light_switch = glutCreateMenu(_light_switch);
    glutAddMenuEntry("Point Light ON", 1);
    glutAddMenuEntry("Point Light OFF", 2);
    glutAddMenuEntry("Directional Light ON", 3);
    glutAddMenuEntry("Directional Light OFF", 4);
    
    int __light_pos = glutCreateMenu(_light_pos);
    glutAddMenuEntry("Invert point light coordinates", 1);
    glutAddMenuEntry("Invert only x-y coordinates of point light", 2);
    glutAddMenuEntry("Invert directional light coordinates", 3);
    glutAddMenuEntry("Invert only x-y coordinates of directional light", 4);
    glutAddMenuEntry("Display the coordinates of the light sources on Terminal/cmd-line",5);
    glutAddMenuEntry("Light moves with the ball",6);
    glutAddMenuEntry("Light is static",7);

	glutCreateMenu(main_menu);
	glutAddSubMenu("Colors", _color_menu);
	glutAddSubMenu("Drawing Style", _draw_type);
    glutAddSubMenu("Shading Types", __shading_types);
    glutAddSubMenu("Consistency",__consistency);
    glutAddSubMenu("Light Switch", __light_switch);
    glutAddSubMenu("Light Coordinates",__light_pos);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}


int main( int argc, char **argv )
{
    ReadPPM_earth();
    ReadPPM_basketball();
    std::cout << "Press h or H for help.\nPress the right mouse click to open the menu.\n" << std::endl;
    glutInit( &argc, argv );
    glutInitDisplayMode(  GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( 512, 512 );
    glutCreateWindow( "Assignment-3 OpenGL" );

    //glewExperimental = GL_TRUE;
    //glewInit();

    std::srand((unsigned)std::time(0));

	init();

    glutDisplayFunc( display ); // set display callback function
	glutReshapeFunc(reshape);
    glutIdleFunc( idle );
//    glutMouseFunc( mouse );
    glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialArrowFunc);

    glutTimerFunc(5,timer,0);
	menuFunc();

    printf("\nGL Version: %s\n", glGetString(GL_VERSION));
	
    fflush(0);

    glutMainLoop();
}
